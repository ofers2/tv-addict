/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tikal.tvaddict;

import com.tikal.tvaddict.dto.TvShowInfo;
import com.tikal.tvaddict.dto.TvShowRss;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class TvShowDataMerge {

	static final String RSS_TOPIC = "rss-topic";
	static final String TV_SHOW_METADATA_TOPIC = "tv-show-metadata";
	static final String TV_INFO_URL = " http://%s:5000/info?movie=%s";

	public static void main(String[] args) throws Exception {
		TvShowDataMerge tvShowDataMerge = new TvShowDataMerge();
		tvShowDataMerge.merge();
	}

	private void merge() throws Exception {
		Properties props = new Properties();
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, "tv-show");
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, Starter.KAFKA_BROKERS);
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

		final StreamsBuilder builder = new StreamsBuilder();

		KStream<String, TvShowRss> right = builder.stream(RSS_TOPIC, Consumed.with(
				Serdes.String(),
				TvShowRssSerdes.getSerdes()
		));


		KStream<String, TvShowInfo> showInfo = right.map((key, tvShowRss) -> {
			TvShowInfo tvShowInfo = new TvShowInfo();
			return KeyValue.pair(key, tvShowInfo);
		});

		showInfo.to(TV_SHOW_METADATA_TOPIC, Produced.with(
				Serdes.String(), ShowInfoSerdes.getSerdes()
		));

		final Topology topology = builder.build();
		final KafkaStreams streams = new KafkaStreams(topology, props);
		final CountDownLatch latch = new CountDownLatch(1);

		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
			@Override
			public void run() {
				streams.close();
				latch.countDown();
			}
		});

		try {
			streams.start();
			latch.await();
		}
		catch (Throwable e) {
			System.exit(1);
		}
		System.exit(0);
	}
}
