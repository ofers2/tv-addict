package com.tikal.tvaddict.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Movie {
	@JsonProperty("imdb_id")
	public String imdbId;
	@JsonProperty("imdb_rate")
	public Float imdbRate;
	@JsonProperty("imdb_title")
	public String imdbTitle;
	@JsonProperty("imdb_year")
	public Integer imdbYear;
}
