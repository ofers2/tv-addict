package com.tikal.tvaddict.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TvShowInfo {
	@JsonProperty("request")
	public Request request;
	@JsonProperty("response")
	public Response response;
	@JsonProperty("status")
	public String status;
}
