package com.tikal.tvaddict.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Response {

	@JsonProperty("movies")
	public List<Movie> movies = null;

}
