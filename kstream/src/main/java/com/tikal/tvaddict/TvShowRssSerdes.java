package com.tikal.tvaddict;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tikal.tvaddict.dto.TvShowRss;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.io.IOException;
import java.util.Map;

public class TvShowRssSerdes implements Serializer<TvShowRss>, Deserializer<TvShowRss> {
    private static TvShowRssSerdes tvShowRssSerdes = new TvShowRssSerdes();
	private static Serde<TvShowRss> serdes = Serdes.serdeFrom(tvShowRssSerdes, tvShowRssSerdes);
	private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

	@Override
	public TvShowRss deserialize(String topic, byte[] data) {
		try {
			return mapper.readValue(data, TvShowRss.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
    public byte[] serialize(String topic, TvShowRss UserGenre) {
        try{
            return mapper.writeValueAsBytes(UserGenre);
        }  catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void close() {

    }

    public static Serde<TvShowRss> getSerdes(){
    	return serdes;
    }
}
