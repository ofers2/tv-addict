package com.tikal.tvaddict;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tikal.tvaddict.dto.TvShowInfo;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.io.IOException;
import java.util.Map;

public class ShowInfoSerdes implements Serializer<TvShowInfo>, Deserializer<TvShowInfo> {
    private static ShowInfoSerdes showInfoSerdes = new ShowInfoSerdes();
	private static Serde<TvShowInfo> serdes = Serdes.serdeFrom(showInfoSerdes, showInfoSerdes);
	private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

	@Override
	public TvShowInfo deserialize(String topic, byte[] data) {
		try {
			return mapper.readValue(data, TvShowInfo.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
    public byte[] serialize(String topic, TvShowInfo UserGenre) {
        try{
            return mapper.writeValueAsBytes(UserGenre);
        }  catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void close() {

    }

    public static Serde<TvShowInfo> getSerdes(){
    	return serdes;
    }
}
