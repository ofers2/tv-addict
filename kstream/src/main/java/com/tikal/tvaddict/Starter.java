package com.tikal.tvaddict;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class Starter {
	private static final String TOPIC_NAME = "tv-show-metadata";
	public static String KAFKA_BROKERS = "broker:29092";

	public static void main(String[] args) {
		System.out.println("Stream is up");
		Starter starter = new Starter();
		starter.go();
	}

	private void go() {
		Producer<Long, String> producer = createProducer();
		ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC_NAME,
				"This is record ");
		try {
			RecordMetadata metadata = producer.send(record).get();
			System.out.println("Record sent with to partition " + metadata.partition()
					+ " with offset " + metadata.offset());
		}
		catch (ExecutionException | InterruptedException e) {
			System.out.println("Error in sending record");
			e.printStackTrace();
		}

	}

	private Producer<Long, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_BROKERS);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		//		props.put(ProducerConfig.ACKS_CONFIG, "all");
		//props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, CustomPartitioner.class.getName());
		return new KafkaProducer<>(props);
	}
}
