package com.tikal.tvaddict;

import com.apptastic.rssreader.Item;
import com.apptastic.rssreader.RssReader;
import lombok.SneakyThrows;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Stream;

public class RssProducer {
    private Set<String> cache = new HashSet<>();

    public static void main(String[] args) {
        new RssProducer().go();
    }

    public void go() {
        String rssUrl  = System.getenv("RSS_URL");
        if (rssUrl == null || rssUrl.isBlank()) {
            rssUrl = "https://showrss.info/other/shows.rss";
        }

        String topicName = System.getenv("TOPIC_NAME");
        if (topicName == null || topicName.isBlank()) {
            topicName = "rss-topic";
        }

        String kafkaBrokers = System.getenv("KAFKA_BROKERS");
        if (kafkaBrokers == null || kafkaBrokers.isBlank()) {
            kafkaBrokers = "localhost:29092";
        }

        System.out.println("Execution params - RSS_URL: " + rssUrl + ", TOPIC_NAME:" + topicName + ", KAFKA_BROKERS: " + kafkaBrokers);
        produceData(rssUrl, topicName, createProducer(kafkaBrokers));
    }

    private Producer<Long, RssData> createProducer(String kafkaBrokers) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }

    @SneakyThrows
    public void produceData(String url, String topicName, Producer<Long, RssData> producer) {
        RssReader reader = new RssReader();
        Stream<Item> rssFeed = reader.read(url);

        int size = cache.size();

        rssFeed.filter(item -> cache.add(item.getGuid().get()))
                .map(item -> new ProducerRecord<Long, RssData>(topicName, new RssData(item.getTitle().get(), item.getPubDate().get())))
                .forEach(record -> producer.send(record));

        System.out.println("Produced " + (cache.size() - size) + " new entries");
    }
}
