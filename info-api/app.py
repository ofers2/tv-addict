import os, logging, json
from flask import Flask, Blueprint, request
from werkzeug.exceptions import HTTPException
from imdb import IMDb


service  = Blueprint('service', __name__)

log_level = os.environ.get('LOG_LEVEL', 'INFO').upper()
logging.basicConfig(format='[%(asctime)s] [%(levelname)s] [%(filename)s:%(lineno)d] %(message)s', level=log_level)

log = logging.getLogger(__name__)

app = Flask(__name__)
ia = IMDb()


@app.errorhandler(HTTPException)
def handle_bad_request(error):
    return error.description, error.code


def get_imdb_info(movie):
    movies = ia.search_movie(movie, results=1)
    info = []
    for movie in movies:
        movie = ia.get_movie(movie.movieID)
        info.append({
            'imdb_id': movie.movieID,
            'imdb_title': movie.get('title'),
            'imdb_year': movie.get('year'),
            'imdb_rate': movie.get('rating')
        })
    return info


@app.route('/info', methods=['GET'])
def get_info():
    movie = request.args.get('movie')

    ret = {
        'request': {
            'movie': movie
        },
        'response': {
            'movies': []
        },
        'status': 'ok',
    }

    if movie:
        try:
            ret['response']['movies'] = get_imdb_info(movie)
        except:
            log.error('Failed to get info for %s', movie, stack_info=True)
            ret['status'] = 'error'
            ret['message'] = f'Failed to get info for {movie}. See more info in the server log.'

    else:
        ret['status'] = 'error'
        ret['message'] = 'Missing movie query parameter'

    log.debug(ret)
    return ret


def main():
    log.info('App started')
    app.run(host='0.0.0.0', port=5000, debug=log.level <= logging.DEBUG)


if __name__ == '__main__':
    main()